package de.mpg.mis.library.fami2;

import de.mpg.mis.library.Krabbelgruppe.Jabberwock;
import de.mpg.mis.library.Krabbelgruppe.JabberwocksErbe;

public class JabberTest 
{

	public JabberTest() 
	{
	}
	
	public static void main(String[] args)
	{
//		Jabberwock[] jabs=new Jabberwock[]{new Jabberwock(),new Jabberwock(),new Jabberwock(),new Jabberwock()};
		Jabberwock[] jabs=new Jabberwock[]{new JabberwocksErbe(2),new JabberwocksErbe(1),new JabberwocksErbe(5),new JabberwocksErbe(2)};
//		jabs[0].feedJabberwock();
		for(int nr=0;nr<jabs.length;nr++)
		{
		  System.out.println("Jabberwock Nr. "+nr+":\n");
		  Jabberwock jab=jabs[nr];
		  for(int opfer=0;opfer<nr+1;opfer++)
		  {
			System.out.print("\tVerfütterung des "+(opfer+1)+"ten Opfers: ");
			jab.feedJabberwock();
		  }
		  System.out.println();
		}
		for(int n=0;n<jabs.length;n++)
		{
		  System.out.println("Jabberwock nr. "+n+":");
		  jabs[n].showAttributes();
		}
	}

}
