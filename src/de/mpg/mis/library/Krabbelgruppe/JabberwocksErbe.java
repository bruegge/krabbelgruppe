package de.mpg.mis.library.Krabbelgruppe;

import de.mpg.mis.library.fami1.Grammatik;

public class JabberwocksErbe extends Jabberwock 
{
	protected int saettigungsschwelle;
	protected int mageninhalt=0;

	public JabberwocksErbe(int wievieleMussDerFressenUmSattZuWerden) 
	{
		saettigungsschwelle=wievieleMussDerFressenUmSattZuWerden;
		setHungrig();
	}
	
	public void feedJabberwock()
	{
		super.feedJabberwock();
		mageninhalt++;
		setHungrig();
	}
	
	public void verdaue()
	{
		mageninhalt--;
		if(mageninhalt<0)
		{
			mageninhalt=0;
		}
		setHungrig();
	}
	
	protected void setHungrig()
	{
		hungrig=(mageninhalt<saettigungsschwelle);		
	}
	
	public String toString()
	{
		int differenz=saettigungsschwelle-mageninhalt;
		String davobastel="";
		if (hungrig)
		{
			davobastel="Der Jabberwock muss noch "+Grammatik.grammatisiere("Person", differenz, "en")+" essen, um satt zu sein.\n";
		}
		return super.toString()+davobastel;
		
	}
}
