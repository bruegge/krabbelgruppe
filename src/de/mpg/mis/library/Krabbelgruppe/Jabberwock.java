package de.mpg.mis.library.Krabbelgruppe;

import de.mpg.mis.library.fami1.Grammatik;

public class Jabberwock {
	String farbe, geschlecht;
//	String geschlecht;
	boolean hungrig;
	int opfer;
	static int wieviele;
	
	public Jabberwock()
	{
		
	}
	
	public void feedJabberwock() 
	{
		if (hungrig == true)
		{
			System.out.println("Yummy ein Mensch.");
			hungrig = false;
			wieviele++;
			opfer++;
		 }
		 else
		 {
			System.out.println("Nein danke, ich hatte schon.");
		}
	}
	
	public String toString()
	{
		StringBuilder sb=new StringBuilder();
		sb.append("Das ist ein " + geschlecht + ", " + farbe + " Jabberwock.\n");
	    if (hungrig == true)
	    {
	    	if(opfer == 0)
	    	{
		      sb.append("Der Jabberwock ist hungrig.\n");
	    	 }
	    	 else
	    	 {
	    	   sb.append("Der Jabberwock ist hungrig, obwohl er schon " + Grammatik.grammatisiere("Mensch", opfer, "en") + " gegessen hat.\n");
	    	}
	     }
	     else
	     {
		   sb.append("Der Jabberwock ist voll, weil er schon " + Grammatik.grammatisiere("Mensch", opfer, "en") + " gegessen hat.\n");
		}
	    return sb.toString();
	}

	public void showAttributes()  
	{
       System.out.print(toString());
	}

	
	
	
	/*
	public static void main (String[] args) {
		Jabberwock j = new Jabberwock();
		j.farbe = "gr�ner";
		j.geschlecht = "m�nnlicher";
		j.hungrig = true;
		System.out.println("Calling showAttributes ...");
		j.showAttributes();
		System.out.println("-----");
		System.out.println("Feeding the Jabberwock ...");
		j.feedJabberwock();
		System.out.println("\n-----\n");
		System.out.println("Calling showAttributes ...");
		j.showAttributes();
		System.out.println("-----");
		System.out.println("Feeding the Jabberwock ...");
		j.feedJabberwock();
		System.out.println("Der Jabberwock hat " + wieviele + "x gegessen.");
		
	}
	*/
}



