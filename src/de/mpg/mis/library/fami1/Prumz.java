package de.mpg.mis.library.fami1;

public class Prumz 
{

	public Prumz() 
	{
	}
	
	public static void main(String[] args)
	{
	  Prumzessin prinzessin;
	  prinzessin=new Prumzessin(1,"Lena");
	  System.out.println(prinzessin.ueberDiePrinzessin());
	  Prumzessin jessica=new Prumzessin(true,new Boolean(false),"Hannelore",15,"Lenchen");
	  System.out.println(jessica.ueberDiePrinzessin());
	  Prumzessin jarry=new Prumzessin(true,null,"Cho",15,"Chang");
	  System.out.println(jarry.ueberDiePrinzessin());
      
	}

	public static void main0(String[] args) 
	{
      Prumzessin prinzessin;
//      new Prumzessin(new Integer(18));
      prinzessin=new Prumzessin();
      System.out.println("Es gibt jetzt "+Prumzessin.wieviele+" Prinzessinen.");
      System.out.println("Die Prinzessin "+prinzessin.name+" nennt sich gern "+prinzessin.nickname+" und ist "+prinzessin.alter+" Monate alt. Sie ist eitel:"+prinzessin.eitel+" und sie ist frech: "+prinzessin.frech+".");
      
      System.out.println(prinzessin.ueberDiePrinzessin());
      
      prinzessin.alter=10;
      prinzessin.name="Marie";
      prinzessin.nickname="Mariechen";
      System.out.println("Die Prinzessin "+prinzessin.name+" nennt sich gern "+prinzessin.nickname+" und ist "+prinzessin.alter+" Monate alt.");
      System.out.println(prinzessin.ueberDiePrinzessin());
      prinzessin.alter=15;
      System.out.println("Die Prinzessin "+prinzessin.name+" nennt sich gern "+prinzessin.nickname+" und ist "+prinzessin.alter+" Monate alt.");

      System.out.println(prinzessin.ueberDiePrinzessin());
      
      prinzessin.alter=10;
      System.out.println("Die Prinzessin "+prinzessin.name+" nennt sich gern "+prinzessin.nickname+" und ist "+prinzessin.alter+" Monate alt.");

      System.out.println(prinzessin.ueberDiePrinzessin());
      
      System.out.println("Jetzt die nächste Prinzessin mit anderem Konstruktor:");
      Prumzessin jessica=new Prumzessin(4, "Jessica");
      Prumzessin egal=new Prumzessin(true,new Boolean(true),"Maria",20,"Jessica");
//      jessica.name="Jessica";
      
      System.out.println("Die andere Prinzessin "+jessica.name+" nennt sich gern "+jessica.nickname+" und ist "+jessica.alter+" Monate alt. Sie ist eitel:"+jessica.eitel+" und sie ist frech: "+jessica.frech+".");

      System.out.println(jessica.ueberDiePrinzessin());
      
      System.out.println("Die unerhebliche Prinzessin "+egal.name+" nennt sich gern "+egal.nickname+" und ist "+egal.alter+" Monate alt. Sie ist eitel:"+egal.eitel+" und sie ist frech: "+egal.frech+".");

      System.out.println(egal.ueberDiePrinzessin());
      
      System.out.println("Es gibt jetzt "+Prumzessin.wieviele+" Prinzessinen.");
	}

}
