package de.mpg.mis.library.fami1;

public class Grammatik 
{
  static String neinWort="nicht";
  
  public Grammatik()
  {
  }
  
  public static String grammatisiere(String singularStamm,String pluralStamm,int anzahl,String pluralEndung,String singularEndung)
  {
	return anzahl+" "+grammatisiere(singularStamm,pluralStamm,anzahl!=1,pluralEndung,singularEndung);
  }
  
  public static String grammatisiere(String wortStamm,int anzahl,String pluralEndung)
  {
	return grammatisiere(wortStamm,null,anzahl,pluralEndung,"");
  }
  
  public static String grammatisiere(String wortStamm,boolean isPlural,String pluralEndung)
  {
	return grammatisiere(wortStamm,null,isPlural,pluralEndung,"");
  }

  public static String grammatisiere(String singularStamm,String pluralStamm,boolean isPlural,String pluralEndung,String singularEndung)
  {
	if(pluralStamm==null || pluralStamm.equals(""))
	{
	  pluralStamm=singularStamm;
	}
	if(singularEndung==null)
	{
      singularEndung="";
	}
	String aus;
	if(isPlural)
	{
	  aus=pluralStamm+pluralEndung;
	 }
	 else
	 {
	   aus=singularStamm+singularEndung;
	}
	return aus;
  }
  
  public static String verneine(String template,String wahrheitsPlatzhalter,Boolean wahrheit)
  {
	if(wahrheit==null)
	{
	  return "";
	}
	String aus;
	if(wahrheit.booleanValue())
	{
	  aus=template.replace(wahrheitsPlatzhalter," ");
	 }
	 else
	 {
	   aus=template.replace(wahrheitsPlatzhalter," "+neinWort+" ");
	}
	return aus;
	/*
	if(wahrheit.booleanValue())
	{
	  return template.replace(wahrheitsPlatzhalter," ");
	}
	return template.replace(wahrheitsPlatzhalter," "+neinWort+" ");
	*/
  }
}
