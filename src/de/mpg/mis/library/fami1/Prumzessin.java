package de.mpg.mis.library.fami1;

public class Prumzessin 
{
	public int alter=1;
	public boolean eitel;
	public Boolean frech; 
	protected String name;
	String nickname;
	private Prumzessin[] schwestern;
	
	public static int wieviele;

	public Prumzessin() 
	{
//	  wieviele++;
	  wieviele=wieviele+1;
      int alter=15;
	  alter=2;
	}

//	protected Prumzessin(Integer neuer)
//	{
//		wieviele++;
//		wieviele++;
//		this.alter=alter;
//	}

	
	public Prumzessin(int alter, String nam)
	{
		wieviele++;
		this.alter=alter;
		name=nam;
	}
	
	public Prumzessin(boolean eitel,Boolean frech,String taufname,int alterDerPrinzessin,String spitzname)
	{
	  wieviele++;
	  this.eitel=eitel;
	  this.frech=frech;
	  name=taufname;
	  alter=alterDerPrinzessin;
	  nickname=spitzname;
	}
	
	public String ueberDiePrinzessin()
	{
	  String aus="Die Prinzessin heißt "+name;
	  if(nickname!=null)
	  {
		aus=aus+", nennt sich gerne "+nickname;
	   }
	   else
	   {
		 aus=aus+", hat noch keinen Spitznamen";  
	  }
	  String monate=Grammatik.grammatisiere("Monat",alter,"e");
	  /*
	  String monate="Monate";
	  if(alter==1)
	  {
		monate="Monat";
	  }
	  */
	  aus=aus+" und ist "+alter+" "+monate+" alt. ";
	  /*
	  String nicht=" nicht ";
	  if(eitel) // if(eitel==true)
	  {
		nicht=" ";
	  }
	  aus=aus+" Sie ist"+nicht+"eitel";
	  */
	  aus=aus+Grammatik.verneine("Sie ist#eitel","#",eitel);
	  String nicht;
	  if(frech!=null)
	  {
		nicht=" nicht ";
		if(frech.equals(true))
		{
		  nicht=" ";
		}
		String und=" und";
		if(!frech.equals(eitel))
		{
		  und=" aber";
		}
		aus=aus+und+nicht+"frech";
	  }
	  aus=aus+".";
	  String anzahl=Grammatik.grammatisiere("Prinzessin",wieviele,"nen");
	  /*
	  String anzahl="Prinzessinnen";
	  if(wieviele==1)
	  {
		anzahl="Prinzessin";  
	  }
	  */
	  aus=aus+"\nSie ist eine von "+wieviele+" "+anzahl+".";
	  return "\n----\n"+aus+"\n----\n";
	}
	
}
